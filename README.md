# Simple PHP RESTful API Framework
This project provides a copy-paste framework to start a project requiring a RESTful api backend service avaible through ajax requests.
## Nginx Config
This is the simplest nginx config to get this working. This includes phpmyadmin which you can remove.
```
server {
        listen 80;
        server_name  localhost;
        index  index.php index.html;
        root   /path/to/project/public_html/;

        location / {
            root   /path/to/project/public_html/web;
            try_files $uri $uri/ =404;
            location ~ \.php$ {
                fastcgi_pass   unix:/var/run/php-fpm/php-fpm.sock;
                fastcgi_index  index.php;
                include        fastcgi.conf;
            }
        }

        location /api {
            try_files $uri /api/index.php?$args;

            location ~ \.php$ {
                fastcgi_pass   unix:/var/run/php-fpm/php-fpm.sock;
                fastcgi_index  index.php;
                include        fastcgi.conf;
            }
        }

        location /phpmyadmin {
            root /usr/share/webapps/;

            location ~ \.php$ {
                fastcgi_pass   unix:/var/run/php-fpm/php-fpm.sock;
                fastcgi_index  index.php;
                include        fastcgi.conf;
            }
        }

    }
```
## Database setup
All that is required is a simple mysql database with a minimum of one table called "users". The password is hashed with password_hash() with the PASSWORD_DEFAULT flag.
### Users Table
|ID (Primary Key)|username   |password   |
|----------------|-----------|-----------|
|1               |username   |[hashed password]|

## Basic Api Example
```php
require_once 'vendor/autoload.php';
use TheKiwiClick\Api\Api;
use TheKiwiClick\Api\Response;
$api = new Api("domain.com", "dbserver.local", "dbuser", "dbpass", "dbname", true); // Last is debug true/false (default is false)

// Route
/*
    Methods are:
    $api->get(...)
    $api->post(...)
    $api->put(...)
    $api->delete(...)

    Authorization levels:
    Api::$ADMIN_ACCESS // user_id = 1
    Api::$USER_ACCESS // any logged in user
    Api::$PUBLIC_ACCESS // any request allowed
*/
$api->put("/users", function ($request) {
    $request->get_input(); // Any form-data/urlencoded data in array form
    return new Response(201, "Created user", ["key"=>"value"]);
}, Api::$USER_ACCESS);

// Handle the request
$api->run();
```
To see all the request data available have a look at the Request class.

All api endpoints MUST return a Response object.

## Front End
For the front end, you can place any custom routing system and static files in any format you want inside the /web folder which is the entry point from the app. From there, you can call the api with requests to the /api route.

### Basic front end XHR request from form
```html
<form>
    <input type="text" name="username" placeholder="Username...">
    <input type="password" name="password" placeholder="Password...">
    <input type="submit" id="submit" value="Create User">
</form>

<script>
    document.querySelector("#submit").addEventListener("click", function (e) {
        // Stop form from auto-reloading page
        e.preventDefault();

        // Create request
        let xhr = new XMLHttpRequest();
        xhr.open("PUT", "/api/users");
        
        // One-time auth (returns a httpOnly cookie)
        xhr.setRequestHeader("Authorization", "Basic " + window.btoa("username2:password2"));
        xhr.withCredentials = true;

        // Send request with form-data using the FormData object
        xhr.send(new FormData(document.querySelector("form")));

        /*
            if sending using put or delete, the Content-Type header must be set to application/x-www-form-urlencoded
            xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");

        */

        xhr.onloadend = function (e) {
            console.log(e);
        };
    });
</script>
```

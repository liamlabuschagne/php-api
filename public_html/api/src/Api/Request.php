<?php
namespace TheKiwiClick\Api;

class Request
{
    private $method;
    private $uri; // Path without query parameters
    private $uri_parameters; // variables passed as a part of the path /users/:id
    private $query_parameters; // GET variables ?key=value
    private $input_parameters; // either form-data for post or x-www-form-urlencoded for put and delete
    private $headers; // all http headers
    private $authorization_level;
    private $user_id;

    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->uri = $this->get_uri();
        $this->uri_parameters = []; // Set by router
        $this->query_parameters = $_GET;
        $this->input_parameters = $this->get_input_parameters();
        $this->headers = \getallheaders();
        $this->authorization_level = Api::$PUBLIC_ACCESS;
    }

    public function get_method()
    {
        return $this->method;
    }

    public function get_uri()
    {
        $this->uri = substr($_SERVER['REQUEST_URI'], 4);
        $this->uri = parse_url($this->uri, \PHP_URL_PATH);

        // Remove any trailing /
        if (strlen($this->uri) > 1 && $this->uri[strlen($this->uri) - 1] == "/") {
            $this->uri = substr($this->uri, 0, strlen($this->uri) - 1);
        } else if (strlen($this->uri) == 0) {
            $this->uri = "/";
        }

        return $this->uri;
    }

    public function get_uri_parameters()
    {
        return $this->uri_parameters;
    }

    public function get_query_parameters()
    {
        return $this->query_parameters;
    }

    public function get_input_parameters()
    {
        // Get input data based on method
        if ($this->method == "POST") { // application/form-data
            $this->input_parameters = $_POST;
        } else if ($this->method == "PUT" || $this->method == "DELETE") { // application/x-www-form-urlencoded
            mb_parse_str(file_get_contents("php://input"), $this->input_parameters);
        }

        return $this->input_parameters;
    }

    public function get_headers()
    {
        return $this->headers;
    }

    public function get_authorization_level()
    {
        return $this->authorization_level;
    }

    public function get_user_id()
    {
        return $this->user_id;
    }

    public function set_uri_parameters(array $uri_parameters)
    {
        $this->uri_parameters = $uri_parameters;
    }

    public function set_authorization_level(int $authorization_level)
    {
        $this->authorization_level = $authorization_level;
    }

    public function set_user_id(int $user_id)
    {
        $this->user_id = $user_id;
    }
}

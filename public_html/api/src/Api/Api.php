<?php
namespace TheKiwiClick\Api;

class Api
{
    private $request;
    private $router;
    private $response;
    private $authentication;

    static $DOMAIN;
    static $DEBUG;

    static $ADMIN_ACCESS = 0;
    static $USER_ACCESS = 1;
    static $PUBLIC_ACCESS = 2;

    public function __construct($domain, $dbhost, $dbusername, $dbpassword, $dbname, $debug = false)
    {
        Database::$host = $dbhost;
        Database::$username = $dbusername;
        Database::$password = $dbpassword;
        Database::$dbname = $dbname;

        Api::$DOMAIN = $domain;
        Api::$DEBUG = $debug;

        // CORS Preflight
        if (Api::$DEBUG) {
            header('Access-Control-Allow-Origin: http://' . $_SERVER['HTTP_HOST']);
        } else {
            header('Access-Control-Allow-Origin: https://' . Api::$DOMAIN);
        }
        header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE');
        header('Access-Control-Allow-Headers: Content-Type');
        header('Content-Type: application/json');

        $this->request = new Request();
        $this->authentication = new Authentication($this->request);
        if ($this->authentication->get_authorization_level() == -1) {
            $this->response = new Response("401", "Credentials Invalid", []);
        }
        $this->router = new Router($this->request);
    }

    public function run()
    {
        if (!isset($this->response)) {
            $this->response = $this->router->run();
        }
        \http_response_code($this->response->get_response_code());
        echo $this->response->to_json();
    }

    public function post(string $uri_pattern, \Closure $action, int $authorization_level)
    {
        $this->router->add_route("POST", $uri_pattern, $action, $authorization_level);
    }

    public function get(string $uri_pattern, \Closure $action, int $authorization_level)
    {
        $this->router->add_route("GET", $uri_pattern, $action, $authorization_level);
    }

    public function put(string $uri_pattern, \Closure $action, int $authorization_level)
    {
        $this->router->add_route("PUT", $uri_pattern, $action, $authorization_level);
    }

    public function delete(string $uri_pattern, \Closure $action, int $authorization_level)
    {
        $this->router->add_route("DELETE", $uri_pattern, $action, $authorization_level);
    }
}

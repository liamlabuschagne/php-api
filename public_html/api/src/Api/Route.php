<?php
namespace TheKiwiClick\Api;

class Route
{
    private $method;
    private $uri_pattern;
    private $action;
    private $authorization_level;

    public function __construct(string $method, string $uri_pattern, \Closure $action, int $authorization_level)
    {
        $this->method = $method;
        $this->uri_pattern = $uri_pattern;
        $this->action = $action;
        $this->authorization_level = $authorization_level;
    }

    public function get_method()
    {
        return $this->method;
    }

    public function get_uri_pattern()
    {
        return $this->uri_pattern;
    }

    public function get_action()
    {
        return $this->action;
    }

    public function get_authorization_level()
    {
        return $this->authorization_level;
    }
}

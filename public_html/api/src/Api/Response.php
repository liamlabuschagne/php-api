<?php
namespace TheKiwiClick\Api;

class Response
{
    private $response_code;
    private $message;
    private $data;

    public function __construct(int $response_code, string $message, array $data)
    {
        $this->response_code = $response_code;
        $this->message = $message;
        $this->data = $data;
    }

    public function get_response_code()
    {
        return $this->response_code;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function get_data()
    {
        return $this->data;
    }

    public function to_json()
    {
        return \json_encode(["message" => $this->message, "data" => $this->data]);
    }
}

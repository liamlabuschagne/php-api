<?php
namespace TheKiwiClick\Api;

class Authentication
{
    private $authorization_level;
    private $user_id;

    public function __construct(Request $request)
    {
        $headers = $request->get_headers();
        if (isset($headers['Authorization']) || isset($_COOKIE['credentials'])) {

            if (isset($headers['Authorization'])) {
                $credentials = preg_replace("#Basic #", "", $headers["Authorization"]);
            } else if (isset($_COOKIE['credentials'])) {
                $credentials = preg_replace("#Basic #", "", $_COOKIE['credentials']);
            }

            $this->authenticate($credentials);
            $request->set_authorization_level($this->authorization_level);
            if ($this->user_id !== null) {
                $request->set_user_id($this->user_id);
            }
        }
    }

    public function authenticate($credentials)
    {
        // Basic Authentication
        $credentials = \base64_decode($credentials);
        $exploded = explode(":", $credentials);
        $username = $exploded[0];
        $password = $exploded[1];

        $dbc = Database::get_connection();
        $stmt = $dbc->prepare("SELECT id,password FROM users WHERE username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt->bind_result($id, $password_hash);
        $stmt->store_result();
        $stmt->fetch();
        if ($stmt->num_rows < 1) {
            $this->authorization_level = -1; // Login failed;
            return;
        }
        $stmt->close();

        if (password_verify($password, $password_hash)) {
            $this->user_id = $id;
            $this->authorization_level = Api::$USER_ACCESS;
            if ($this->user_id == 1) {
                $this->authorization_level = Api::$ADMIN_ACCESS;
            }

            if (!isset($_COOKIE['credentials'])) {
                if (Api::$DEBUG) {
                    setcookie("credentials", \base64_encode($username . ":" . $password), time() + 3600, "/", $_SERVER['HTTP_HOST'], false, true);
                } else {
                    setcookie("credentials", \base64_encode($username . ":" . $password), time() + 3600, "/", Api::$DOMAIN, true, true);
                }
            }

        } else {
            $this->authorization_level = -1; // Login failed;
        }
    }

    public function get_authorization_level()
    {
        return $this->authorization_level;
    }
}

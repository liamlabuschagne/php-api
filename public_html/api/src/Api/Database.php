<?php
namespace TheKiwiClick\Api;

class Database
{
    static $host;
    static $username;
    static $password;
    static $dbname;

    public static function get_connection()
    {
        return new \mysqli(Database::$host, Database::$username, Database::$password, Database::$dbname);
    }
}

<?php
namespace TheKiwiClick\Api;

class Router
{
    private $request;
    private $routes = [];
    private $response;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function run(): Response
    {
        $uri_parameters = [];
        $match_found = false;

        // Search through every route in the routes array
        foreach ($this->routes as $route) {

            if ($route->get_method() != $this->request->get_method()) {
                continue;
            }

            // Split both the route uri pattern and the request uri by / to compare
            $route_uri_segments = explode("/", $route->get_uri_pattern());
            $request_uri_segments = explode("/", $this->request->get_uri());

            // Check they have the same number of segments
            if (count($route_uri_segments) == count($request_uri_segments)) {
                // Loop through each segment to see wether they match
                foreach ($route_uri_segments as $key => $route_segment) {
                    // If they don't match, it must either be a uri parameter or it is not a match
                    if ($route_segment != $request_uri_segments[$key]) {
                        // If the route segment is a uri parameter then store that in a local variable
                        if ($route_segment[0] == ":") {
                            $match_found = true;
                            $uri_parameters[substr($route_segment, 1)] = $request_uri_segments[$key];
                        } else {
                            $match_found = false;
                            break;
                        }
                    } else {
                        $match_found = true;
                    }
                }

                // We've checked this route
                if ($match_found) {
                    if ($route->get_authorization_level() < $this->request->get_authorization_level()) {
                        return new Response(401, "This route requires a higher level of authorization.", []);
                    }

                    // append uri parameters to request
                    $this->request->set_uri_parameters($uri_parameters);

                    // execute route action
                    return $route->get_action()($this->request); // This will return a Response
                }
            }
        }
        return new Response(404, "Route not found.", []);
    }

    public function add_route(string $method, string $uri_pattern, \Closure $action, int $authorization_level)
    {
        $this->routes[] = new Route($method, $uri_pattern, $action, $authorization_level);
    }
}
